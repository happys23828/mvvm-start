package com.example.happy.mvvmexample.Main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    lateinit var mViewPresnter: MainActivityView
    lateinit var mNavigator: MainActivityNavigator
    var showToast: MutableLiveData<Int>? = MutableLiveData()
    var count = 0

    fun setNavigator(mNavigator: MainActivityNavigator) {
        this.mNavigator = mNavigator
    }


    fun setViewPresnter(mViewPresnter: MainActivityView) {
        this.mViewPresnter = mViewPresnter
    }

    fun changeText(changeText: String) {
        mViewPresnter.onTextChange(changeText)
        Thread.sleep(3000)
        count++
        mNavigator.openSecondActivity()
        showToast!!.value = count

    }
}