package com.example.happy.mvvmexample.Main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.happy.mvvmexample.R
import com.example.happy.mvvmexample.SecondActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),MainActivityView ,MainActivityNavigator{

    lateinit var mMainActivityViewModel : MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setMvvm()

        btn.setOnClickListener {
            mMainActivityViewModel.changeText("Mayank")
        }


        mMainActivityViewModel.showToast!!.observe(this, Observer {
            if(it!!%2==0){
                Toast.makeText(this,"Yeah Cool Live Data ..... $it ",Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun setMvvm() {
        mMainActivityViewModel =ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        mMainActivityViewModel.setViewPresnter(this)
        mMainActivityViewModel.setNavigator(this)

    }

    override fun onTextChange(text: String) {
        text_view.text = text
    }

    override fun openSecondActivity() {
        startActivity(Intent(this,SecondActivity::class.java))
    }
}
