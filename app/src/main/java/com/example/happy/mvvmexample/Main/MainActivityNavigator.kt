package com.example.happy.mvvmexample.Main

interface MainActivityNavigator {
    fun openSecondActivity()
}